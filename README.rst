================
PowerRodIT Demo
================

Description
===========
This is a demo repository of an automated deployment to k8s. It uses Terraform to create the infrastructure and GitLab's Auto Devops
to deploy using helm and stuff.

It includes:

* ingress-nginx
* cert-bot with an ACME (LetsEncrypt) clusterIssuer
* prometheus

You can visit the website at: https://renich-powerodit-demo.gl.lotoftrouble.com/

