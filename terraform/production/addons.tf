# Wait for cluster
resource "time_sleep" "wait_for_k8s" {
  depends_on = [google_container_cluster.primary]

  create_duration = "2m"
}

# k8s addons
# ingress-nginx
resource "kubernetes_namespace" "gitlab-managed-apps" {
  depends_on = [
    google_container_cluster.primary,
    time_sleep.wait_for_k8s,
  ]

  metadata {
    name = "gitlab-managed-apps"
  }
}

resource "helm_release" "nginx_ingress" {
  depends_on = [
    kubernetes_namespace.gitlab-managed-apps
  ]

  name      = "ingress-nginx"
  namespace = "gitlab-managed-apps"

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
}

## cert-manager
resource "kubernetes_namespace" "cert-manager" {
  depends_on = [
    google_container_cluster.primary,
    time_sleep.wait_for_k8s,
  ]

  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert-manager" {
  depends_on = [
    kubernetes_namespace.cert-manager
  ]

  name      = "cert-manager"
  namespace = "cert-manager"

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"

  set {
    name  = "installCRDs"
    value = "true"
  }

  set {
    name  = "ingressShim.defaultIssuerName"
    value = "letsencrypt-prod"
  }

  set {
    name  = "ingressShim.defaultIssuerKind"
    value = "ClusterIssuer"
  }

  set {
    name  = "ingressShim.defaultIssuerGroup"
    value = "cert-manager.io"
  }

  provisioner "local-exec" {
    command = "kubectl -n cert-manager create -f helm/cert-manager/issuer.yaml"
  }
}

# prometheus
resource "helm_release" "prometheus" {
  depends_on = [
    kubernetes_namespace.gitlab-managed-apps
  ]

  name      = "prometheus"
  namespace = "gitlab-managed-apps"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus"

  values = [
    "${file("helm/prometheus/values.yaml")}"
  ]
}

# sentry
resource "kubernetes_namespace" "sentry" {
  depends_on = [
    google_container_cluster.primary,
    time_sleep.wait_for_k8s,
  ]

  metadata {
    name = "sentry"
  }
}

resource "helm_release" "sentry" {
  depends_on = [
    kubernetes_namespace.sentry,
    google_container_cluster.primary,
  ]

  name = "sentry"
  namespace = "sentry"

  repository = "https://sentry-kubernetes.github.io/charts"
  chart = "sentry"

  values = [
    "${file("helm/sentry/values.yaml")}"
  ]
}
