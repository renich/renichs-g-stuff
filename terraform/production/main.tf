# Project
#resource "google_project" "demo" {
#  name = "powerodit-demo"
#  project_id = "powerodit-demo"
#}

# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = false
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.2.0.0/16"

  secondary_ip_range {
    range_name    = "services-range"
    ip_cidr_range = "192.168.1.0/24"
  }
}

# GKE cluster
resource "google_container_cluster" "primary" {
  depends_on = [
    google_compute_network.vpc,
    google_compute_subnetwork.subnet,
  ]

  name     = "${var.project_id}-gke"
  location = var.region

  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  # addons
  addons_config {
    #istio_config {
    #  disabled = true
    #  auth     = "AUTH_MUTUAL_TLS"
    #}

    horizontal_pod_autoscaling {
      disabled = false
    }
  }

  # autoscaling
  cluster_autoscaling {
    enabled = true

    resource_limits {
      resource_type = "cpu"
      minimum       = 2
      maximum       = 4
    }

    resource_limits {
      resource_type = "memory"
      minimum       = 2
      maximum       = 4
    }
  }

  vertical_pod_autoscaling {
    enabled = true
  }

  # maintenance
  maintenance_policy {
    daily_maintenance_window {
      start_time = "03:00"
    }
  }

  # network policy
  network_policy {
    enabled = true
  }

  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${google_container_cluster.primary.name} --region ${var.region} --project ${var.project_id}"
  }
}

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name       = "${google_container_cluster.primary.name}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  # autoscaling
  autoscaling {
    min_node_count = 1
    max_node_count = 3
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project_id
    }

    preemptible  = true
    machine_type = "n2d-highcpu-4"
    tags         = ["gke-node", "${var.project_id}-gke"]

    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

