#!/usr/bin/env bash

api=$( kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}' )
ca_secret=$( kubectl get secrets | grep default-token | gawk '{ print $1 }' )
ca=$( kubectl get secret $ca_secret  -o jsonpath="{['data']['ca\.crt']}" | base64 --decode )
token=$( kubectl -n kube-system get -o jsonpath='{.data.token}' secret $( kubectl -n kube-system get secret | grep gitlab | gawk '{print $1}' ) | base64 -d )

cat << EOF

Cluster info:

API url: $api

CA:
$ca

token:   $token

